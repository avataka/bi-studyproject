import * as React from 'react'
import { Link } from 'react-router-dom';

import { IDivisionData } from '../../Actions/Models'
import { IDivision } from './../../Reducers/DivisionReducer'
import './DivisionItem.less'

/**
 * Пропсы компонента из стора.
 * @prop {IDivision} division Данные о подразделении.
 * @prop {function} onEdit Запрос на редактирование.
 * @prop {function} onDelete Запрос на удаление.
 * @prop {function} onOpen Запись текущего подразделения, для запроса персонала.
 * @prop {string} organization Название текущей организации, получено из роутера.
 */
interface IProps {
  division: IDivision,
  onEdit: (data: IDivisionData) => void
  onDelete: () => void
  onOpen: () => void
  organization: string
}

/**
 * @class
 * @name DivisionItem
 * @classdesc Конкретное подразделение в списке подразделений.
 */
class DivisionItem extends React.Component<IProps, {}> {

  onEdit = () => {this.props.onEdit({ id: this.props.division.id, id_organization: this.props.division.id_organization, name: this.props.division.name, phone: this.props.division.phone })}

  onDelete = () => {this.props.onDelete()}

  render() {
    const {division} = this.props;

    return <li className="divisionItem">
      <div className="collumn">
        <div>id: {division.id}</div>
        <div>id_organization: {division.id_organization}</div>
        <div>name: {division.name}</div>
        <div>phone: {division.phone}</div>
      </div>
      <Link to={`/organizations/${this.props.organization}/${division.name.replace(/\s/g, '-')}`} onClick={this.props.onOpen} className="btn btn-outline-primary btn-warning btn-sm"> Open </Link>
      <input
        className="btn btn-outline-primary btn-warning btn-sm"
        type="button"
        value="Edit"
        onClick={this.onEdit}
      />
      <input
        className="btn btn-outline-primary btn-warning btn-sm"
        type="button"
        value="Delete"
        onClick={this.onDelete}
      />
    </li>
  }
}

export default DivisionItem;
