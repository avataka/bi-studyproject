import * as React from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IActionType } from '../../common'
import { Actions } from '../../Actions/Actions'
import { ModalEssence, ModalTypes } from './../../Actions/Consts'

import { IOrganization } from './../../Reducers/OrganizationReducer'

import OrganizationItem from './../OrganizationItem/OrganizationItem'

/**
* Пропсы для передачи экшенов.
* @prop {Actions} actions Экшены для работы приложения.
*/
export interface IDispatchProps{
  actions: Actions;
}

/**
 * Пропсы компонента.
 */
interface IProps {
  organizations: IOrganization[]
  //handleAdd: Function
}

type TProps = IDispatchProps & IProps;

class OrganizationList extends React.Component<TProps, {}> {

  render() {
    const {organizations} = this.props;

    return <React.Fragment>
      <ul>
        {organizations.map((organization, index) => <OrganizationItem organization={organization}
                                                                      onOpen={this.props.actions.onDivisionSetId.bind(this, organization.id)}
                                                                      onEdit={this.props.actions.onModalOpen.bind(this, ModalTypes.EDIT, ModalEssence.ORGANIZATION)}
                                                                      onDelete={this.props.actions.onModalOpen.bind(this, ModalTypes.DELETE, ModalEssence.ORGANIZATION, organization)}
                                                                      key={index}/>)}
      </ul>
    </React.Fragment>
  }
}

const connectOrganizationList = connect(
  () => ({}),
  (dispatch: Dispatch<IActionType>): IDispatchProps => ({
      actions: new Actions(dispatch)
  })
)(OrganizationList);

export {connectOrganizationList as OrganizationList};
