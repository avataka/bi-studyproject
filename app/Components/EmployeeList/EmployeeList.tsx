import * as React from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IActionType } from '../../common'
import { Actions } from '../../Actions/Actions'
import { ModalEssence, ModalTypes } from './../../Actions/Consts'

import { IEmployee } from './../../Reducers/EmployeeReducer'

import EmployeeItem from './../EmployeeItem/EmployeeItem'

/**
* Пропсы для передачи экшенов.
* @prop {Actions} actions Экшены для работы приложения.
*/
export interface IDispatchProps{
  actions: Actions;
}

/**
 * Пропсы компонента.
 * @prop {IEmployee[]} employees Массив персонала.
 */
interface IProps {
    employees: IEmployee[]
}

type TProps = IDispatchProps & IProps;

/**
 * @class
 * @name EmployeeList
 * @classdesc Список персонала.
 */
class EmployeeList extends React.Component<TProps, {}> {

  render() {
    const {employees} = this.props;

    return <React.Fragment>
      <ul>
        {employees.map((employee, index) => <EmployeeItem employee={employee}
                                                            onEdit={this.props.actions.onModalOpen.bind(this, ModalTypes.EDIT, ModalEssence.EMPLOYEE)}
                                                            onDelete={this.props.actions.onModalOpen.bind(this, ModalTypes.DELETE, ModalEssence.EMPLOYEE, employee)}
                                                            key={index}/>)}
      </ul>
    </React.Fragment>
  }
}

const connectEmployeeList = connect(
  () => ({}),
  (dispatch: Dispatch<IActionType>): IDispatchProps => ({
      actions: new Actions(dispatch)
  })
)(EmployeeList);

export {connectEmployeeList as EmployeeList};
