import * as React from 'react'
import { Link } from 'react-router-dom';

import { IOrganizationData } from '../../Actions/Models'
import { IOrganization } from './../../Reducers/OrganizationReducer'
import './OrganizationItem.less'

/**
 * Пропсы компонента из стора.
 * @prop {IOrganization} organization Данные об организации.
 * @prop {function} onEdit Запрос на редактирование.
 * @prop {function} onDelete Запрос на удаление.
 * @prop {function} onOpen Запись текущей организации, для запроса подразделений.
 */
interface IProps {
  organization: IOrganization,
  onEdit: (data: IOrganizationData) => void
  onDelete: () => void
  onOpen: () => void
}

/**
 * @class
 * @name OrganizationItem
 * @classdesc Конкретная организация в списке организаций.
 */
class OrganizationItem extends React.Component<IProps, {}> {

  onEdit = () => {this.props.onEdit({ id: this.props.organization.id, name: this.props.organization.name, address: this.props.organization.address, INN: this.props.organization.INN })}

  onDelete = () => {this.props.onDelete()}

  render() {
    const {organization} = this.props;
console.log(organization)
    return <li className="organizationItem">
      <div className="collumn">
        <div>id: {organization.id}</div>
        <div>name: {organization.name}</div>
        <div>address: {organization.address}</div>
        <div>INN: {organization.INN}</div>
      </div>
      <Link to={`/organizations/${organization.name.replace(/\s/g, '-')}`} onClick={this.props.onOpen} className="btn btn-outline-primary btn-warning btn-sm"> Open </Link>
      <input
        className="btn btn-outline-primary btn-warning btn-sm"
        type="button"
        value="Edit"
        onClick={this.onEdit}
      />
      <input
        className="btn btn-outline-primary btn-warning btn-sm"
        type="button"
        value="Delete"
        onClick={this.onDelete}
      />
    </li>
  }
}

export default OrganizationItem;
