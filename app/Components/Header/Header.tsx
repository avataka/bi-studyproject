import * as React from 'react'

import { Authorization } from '../Authorization/Authorization'
import './Header.less'

/**
 * Минималистичный хедер.
 */
const Header: React.FC = () => {
  return (
    <header className="header">
      <Authorization/>
    </header>
  );
}
export default Header
