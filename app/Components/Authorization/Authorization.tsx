import * as React from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IActionType } from '../../common'
import { Actions } from '../../Actions/Actions'
import { IStoreState } from '../../Reducers/Reducers'

import './Authorization.less'

/**
 * Пропсы компонента из стора.
 * @prop {boolean} loginStatus Состояние авторизации пользователя.
 * @prop {boolean} waitingForLogin Ожидание завершения процедуры авторизации (завершение логина).
 * @prop {boolean} isLoginWrong Попытка авторизации провалена.
 */
interface IStateProps{
  loginStatus: boolean;
  waitingForLogin: boolean;
  isLoginWrong: boolean;
}

/**
* Пропсы для передачи экшенов.
* @prop {Actions} actions Экшены для работы приложения.
*/
export interface IDispatchProps{
  actions: Actions;
}

/**
* @interface
* @description Локальный state данных для валидации
* @prop {string} LoginState Введенный логин.
* @prop {string} passwordState Введенный пароль.
* @prop {boolean} loginError Ошибка при валидации логина.
* @prop {boolean} passwordError Ошибка при валидации пароля.
* @prop {boolean} showError Показывать ошибку.
*/
interface IState {
  loginState: string,
  passwordState: string,
  loginError: boolean,
  passwordError: boolean,
  showError: boolean
}

type TProps = IStateProps & IDispatchProps;

/**
 * Компонент авторизации пользователя
 * @class
 */
class Authorization extends React.Component<TProps, IState> {

  constructor(props: TProps) {
    super(props);
    this.state = {
      loginState: '',
      passwordState: '',
      loginError: true,
      passwordError: true,
      showError: false
    };
  }

  /**
   * @method fastLogin Для быстрого входа. Отправляет заведомо верные данные пользователя.
   */
  fastLogin = () => this.props.actions.onLogin({loginData:{login:'alan',password:'123'}});

  /**
   * @method handleLogout Для выхода пользователя.
   */
  handleLogout = () => this.props.actions.onLogout();

  /**
   * @method handleSubmitForm Авторизация. Обрабтка формы.
   */
  handleSubmitForm = (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      if (!this.state.loginError && !this.state.passwordError) {
        const login = this.state.loginState
        const password = this.state.passwordState
        this.setState({showError: false})
        this.props.actions.onLogin({loginData:{login:login,password:password}})
      } else {
        this.setState({showError: true})
      }
  };

  /**
   * @method handleChangeLogin Обработка введенного логина.
   */
  handleChangeLogin = (e: React.FormEvent<HTMLInputElement>) => {
    e.preventDefault()
    const value = String(e.currentTarget.value)
    if((typeof(value) === 'string') && (value.length >= 4)) {
      this.setState({loginState: value})
      this.setState({loginError: false})
    } else {
      this.setState({loginError: true})
    }
  };

  /**
   * @method handleChangePassword Обработка введенного пароля.
   */
  handleChangePassword = (e: React.FormEvent<HTMLInputElement>) => {
    e.preventDefault()
    const value = String(e.currentTarget.value)
    if((typeof(value) === 'string') && (value.length >= 3)) {
      this.setState({passwordState: value})
      this.setState({passwordError: false})
    } else {
      this.setState({passwordError: true})
    }
  };

  render() {
    const {loginStatus, waitingForLogin, isLoginWrong} = this.props;

    return ( <div className={`authorization ${loginStatus ? 'authorization--right': ''}`}>
      {loginStatus ?
        <div className="authorization__right">
          <input
            className="btn btn-outline-primary btn-warning btn-sm"
            disabled={waitingForLogin}
            type="button"
            value="logout"
            onClick={this.handleLogout}
          />
        </div>:
        <React.Fragment>
          <div className="authorization__left">
            <input
              className="btn btn-outline-primary btn-warning btn-sm"
              disabled={waitingForLogin}
              type="button"
              value="FastLoginButton"
              onClick={this.fastLogin}
            />
          </div>
          <form
            className="authorization__form authorization__right"
            action="http://127.0.0.1:8080/athorize"
            method="post"
            name="loginData"
            onSubmit={this.handleSubmitForm}
          >
            {
              waitingForLogin ?
                <p>Авторизация...</p> :''
            }
            <div className="authorization__login">
              <label>Login:<br/>
                <input
                  className="form-control-sm"
                  type="text"
                  name="login"
                  placeholder="alan"
                  required
                  onChange={this.handleChangeLogin}
                />
              </label>
              { this.state.showError ?
                <div className="authorization__error">must be more then 4 symbols</div>:''
              }
              {isLoginWrong ? <div className="authorization__error">Нет доступа</div>: ''}
            </div>
            <div className="authorization__password">
              <label>Password:<br/>
                <input
                  className="form-control-sm"
                  type="password"
                  name="password"
                  placeholder="123"
                  required
                  onChange={this.handleChangePassword}
                />
              </label>
              { this.state.showError ?
                <div className="authorization__error">must be more then 3 symbols</div>:''
              }
            </div>
            <input
              className="btn btn-outline-primary btn-sm authorization__submit"
              type="submit"
              value="Log in"
            />
          </form>
        </React.Fragment>
      }
      </div>
    );
  }
}

const connectAuthorization = connect(
  (state: IStoreState): IStateProps => ({
      loginStatus: state.LoginReducer.loginStatus,
      waitingForLogin: state.LoginReducer.loading,
      isLoginWrong: state.LoginReducer.isLoginWrong
  }),
  (dispatch: Dispatch<IActionType>): IDispatchProps => ({
      actions: new Actions(dispatch)
  })
)(Authorization);

export {connectAuthorization as Authorization};
