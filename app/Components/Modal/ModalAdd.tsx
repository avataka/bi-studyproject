import * as React from 'react'
import { ModalEssence } from '../../Actions/Consts'
import { IDivisionData, IEmployeeData, IOrganizationData } from '../../Actions/Models'

/**
 * @interface
 * @description Пропсы
 */
interface IProps {
    type: ModalEssence,
    action: (data: IOrganizationData|IDivisionData|IEmployeeData) => void,
    onClose: () => void
    data?: any
}

/**
 * @interface
 * @description Локальное хранилище.
 */
interface IState {
    name: string,
    address: string,
    INN: number,
    phone: number,
    FIO: string,
    position: string,
    nameError: boolean,
    addressError: boolean,
    INNError: boolean,
    phoneError: boolean,
    FIOError: boolean,
    positionError: boolean,
    showError: boolean,
}

/**
 * @class
 * @name ModalAdd
 * @classdesc Тело окна добавления.
 */
export default class ModalAdd extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
          name: '',
          address: '',
          INN: 0,
          phone: 0,
          FIO: '',
          position: '',
          nameError: true,
          addressError: true,
          INNError: true,
          phoneError: true,
          FIOError: true,
          positionError: true,
          showError: false
        };
    }

    handleSubmitForm = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (this.props.type === ModalEssence.ORGANIZATION) {
            if (!this.state.nameError && !this.state.addressError && !this.state.INNError) {
                const name = this.state.name
                const address = this.state.address
                const INN = this.state.INN
                this.setState({showError: false})
                this.props.action({name: name, address: address, INN: INN})
                this.props.onClose()
            } else {
                this.setState({showError: true})
            }
        } else if (this.props.type === ModalEssence.DIVISION) {
            if (!this.state.nameError && !this.state.phoneError) {
                const name = this.state.name
                const phone = this.state.phone
                const id_organization = this.props.data.id_organization
                this.setState({showError: false})
                this.props.action({name: name, phone: phone, id_organization: id_organization})
                this.props.onClose()
            } else {
                this.setState({showError: true})
            }
        } else if (this.props.type === ModalEssence.EMPLOYEE) {
            if (!this.state.FIOError && !this.state.addressError && !this.state.positionError) {
                const FIO = this.state.FIO
                const address = this.state.address
                const position = this.state.position
                const id_division = this.props.data.id_division
                this.setState({showError: false})
                this.props.action({FIO: FIO, address: address, position: position, id_division: id_division})
                this.props.onClose()
            } else {
                this.setState({showError: true})
            }
        } else throw 'error'
    };

    handleChangeName = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault()
        const value = String(e.currentTarget.value)
        if((typeof(value) === 'string') && (value.length >= 0)) {
          this.setState({name: value})
          this.setState({nameError: false})
        } else {
          this.setState({nameError: true})
        }
    };

    handleChangeAddress = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault()
        const value = String(e.currentTarget.value)
        if((typeof(value) === 'string') && (value.length >= 0)) {
          this.setState({address: value})
          this.setState({addressError: false})
        } else {
          this.setState({addressError: true})
        }
    };

    handleChangeINN = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault()
        const value = String(e.currentTarget.value)
        if((typeof(value) === 'string') && (value.length >= 0)) {
          this.setState({INN: parseInt(value, 10)})
          this.setState({INNError: false})
        } else {
          this.setState({INNError: true})
        }
    };

    handleChangePhone = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault()
        const value = String(e.currentTarget.value)
        if((typeof(value) === 'string') && (value.length >= 0)) {
          this.setState({phone: parseInt(value, 10)})
          this.setState({phoneError: false})
        } else {
          this.setState({phoneError: true})
        }
    };

    handleChangeFIO = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault()
        const value = String(e.currentTarget.value)
        if((typeof(value) === 'string') && (value.length >= 0)) {
          this.setState({FIO: value})
          this.setState({FIOError: false})
        } else {
          this.setState({FIOError: true})
        }
    };

    handleChangePosition = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault()
        const value = String(e.currentTarget.value)
        if((typeof(value) === 'string') && (value.length >= 0)) {
          this.setState({position: value})
          this.setState({positionError: false})
        } else {
          this.setState({positionError: true})
        }
    };

  render() {
    const { type } = this.props;

    return (
      <div>
          {
            (type === ModalEssence.ORGANIZATION)?
                <form onSubmit={this.handleSubmitForm}>
                    <div className="">
                        <label>Name:<br/>
                            <input
                            className="form-control-sm"
                            type="text"
                            name="name"
                            placeholder="name"
                            required
                            onChange={this.handleChangeName}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div className="">
                        <label>Address:<br/>
                            <input
                            className="form-control-sm"
                            type="text"
                            name="address"
                            placeholder="address"
                            required
                            onChange={this.handleChangeAddress}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div className="">
                        <label>INN:<br/>
                            <input
                            className="form-control-sm"
                            type="number"
                            name="INN"
                            placeholder="INN"
                            required
                            onChange={this.handleChangeINN}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div>
                        <input
                            className="btn btn-outline-primary btn-sm"
                            type="submit"
                            value="create"
                        />
                    </div>
                </form>:
            (type === ModalEssence.DIVISION)?
                <form onSubmit={this.handleSubmitForm}>
                    <div className="">
                        <label>Name:<br/>
                            <input
                            className="form-control-sm"
                            type="text"
                            name="name"
                            placeholder="name"
                            required
                            onChange={this.handleChangeName}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div className="">
                        <label>Phone:<br/>
                            <input
                            className="form-control-sm"
                            type="number"
                            name="phone"
                            placeholder="phone"
                            required
                            onChange={this.handleChangePhone}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div>
                        <input
                            className="btn btn-outline-primary btn-sm"
                            type="submit"
                            value="create"
                        />
                    </div>
                </form>:
            (type === ModalEssence.EMPLOYEE)?
                <form onSubmit={this.handleSubmitForm}>
                    <div className="">
                        <label>FIO:<br/>
                            <input
                            className="form-control-sm"
                            type="text"
                            name="FIO"
                            placeholder="FIO"
                            required
                            onChange={this.handleChangeFIO}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div className="">
                        <label>Address:<br/>
                            <input
                            className="form-control-sm"
                            type="text"
                            name="address"
                            placeholder="address"
                            required
                            onChange={this.handleChangeAddress}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div className="">
                        <label>Position:<br/>
                            <input
                            className="form-control-sm"
                            type="text"
                            name="position"
                            placeholder="position"
                            required
                            onChange={this.handleChangePosition}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div>
                        <input
                            className="btn btn-outline-primary btn-sm"
                            type="submit"
                            value="create"
                        />
                    </div>
                </form>:
                'wrong type'
          }
      </div>
    );
  }
}
