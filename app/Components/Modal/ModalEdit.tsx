import * as React from 'react'
import { ModalEssence } from '../../Actions/Consts'
import { IDivisionData, IEmployeeData, IOrganizationData } from '../../Actions/Models'

/**
 * @interface
 * @description Пропсы
 */
interface IProps {
    type: ModalEssence,
    action: (data: IOrganizationData|IDivisionData|IEmployeeData) => void,
    onClose: () => void,
    data: any/*IOrganizationData|IDivisionData|IEmployeeData*/
}

/**
 * @interface
 * @description Локальное хранилище.
 */
interface IState {
    name: string,
    address: string,
    INN: number,
    phone: number,
    FIO: string,
    position: string,
    nameError: boolean,
    addressError: boolean,
    INNError: boolean,
    phoneError: boolean,
    FIOError: boolean,
    positionError: boolean,
    showError: boolean,
    edited: boolean
}

/**
 * @class
 * @name ModalEdit
 * @classdesc Тело окна редактирования.
 */
export default class ModalEdit extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
          name: typeof this.props.data.name !== 'undefined' ?  this.props.data.name : '',
          address: typeof this.props.data.address !== 'undefined' ?  this.props.data.address : '',
          INN: typeof this.props.data.INN !== 'undefined' ?  this.props.data.INN : 0,
          phone: typeof this.props.data.phone !== 'undefined' ?  this.props.data.phone : 0,
          FIO: typeof this.props.data.FIO !== 'undefined' ?  this.props.data.FIO : '',
          position: typeof this.props.data.position !== 'undefined' ?  this.props.data.position : '',
          nameError: false,
          addressError: false,
          INNError: false,
          phoneError: false,
          FIOError: false,
          positionError: false,
          showError: false,
          edited: false,
        };
    }

    handleSubmitForm = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (this.props.type === ModalEssence.ORGANIZATION) {
            if (!this.state.nameError && !this.state.addressError && !this.state.INNError && this.state.edited) {
                const id = this.props.data.id
                const name = this.state.name
                const address = this.state.address
                const INN = this.state.INN
                this.setState({showError: false})
                this.props.action({id: id, name: name, address: address, INN: INN})
                this.props.onClose()
            } else {
                this.setState({showError: true})
            }
        } else if (this.props.type === ModalEssence.DIVISION) {
            if (!this.state.nameError && !this.state.phoneError && this.state.edited) {
                const id = this.props.data.id
                const id_organization = this.props.data.id_organization
                const name = this.state.name
                const phone = this.state.phone
                this.setState({showError: false})
                this.props.action({id: id, id_organization: id_organization, name: name, phone: phone})
                this.props.onClose()
            } else {
                this.setState({showError: true})
            }
        } else if (this.props.type === ModalEssence.EMPLOYEE) {
            if (!this.state.FIOError && !this.state.addressError && !this.state.positionError && this.state.edited) {
                const id = this.props.data.id
                const id_division = this.props.data.id_division
                const FIO = this.state.FIO
                const address = this.state.address
                const position = this.state.position
                this.setState({showError: false})
                this.props.action({id: id, id_division: id_division, FIO: FIO, address: address, position: position})
                this.props.onClose()
            } else {
                this.setState({showError: true})
            }
        } else throw 'error'
    };

    handleChangeName = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault()
        this.setState({edited: true})
        const value = String(e.currentTarget.value)
        if((typeof(value) === 'string') && (value.length >= 0)) {
          this.setState({name: value})
          this.setState({nameError: false})
        } else {
          this.setState({nameError: true})
        }
    };

    handleChangeAddress = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault()
        this.setState({edited: true})
        const value = String(e.currentTarget.value)
        if((typeof(value) === 'string') && (value.length >= 0)) {
          this.setState({address: value})
          this.setState({addressError: false})
        } else {
          this.setState({addressError: true})
        }
    };

    handleChangeINN = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault()
        this.setState({edited: true})
        const value = String(e.currentTarget.value)
        if((typeof(value) === 'string') && (value.length >= 0)) {
          this.setState({INN: parseInt(value, 10)})
          this.setState({INNError: false})
        } else {
          this.setState({INNError: true})
        }
    };

    handleChangePhone = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault()
        this.setState({edited: true})
        const value = String(e.currentTarget.value)
        if((typeof(value) === 'string') && (value.length >= 0)) {
          this.setState({phone: parseInt(value, 10)})
          this.setState({phoneError: false})
        } else {
          this.setState({phoneError: true})
        }
    };

    handleChangeFIO = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault()
        this.setState({edited: true})
        const value = String(e.currentTarget.value)
        if((typeof(value) === 'string') && (value.length >= 0)) {
          this.setState({FIO: value})
          this.setState({FIOError: false})
        } else {
          this.setState({FIOError: true})
        }
    };

    handleChangePosition = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault()
        this.setState({edited: true})
        const value = String(e.currentTarget.value)
        if((typeof(value) === 'string') && (value.length >= 0)) {
          this.setState({position: value})
          this.setState({positionError: false})
        } else {
          this.setState({positionError: true})
        }
    };

  render() {
    const { type } = this.props;

    return (
      <div>
          {
            (type === ModalEssence.ORGANIZATION)?
                <form onSubmit={this.handleSubmitForm}>
                    <div className="">
                        <label>Name:<br/>
                            <input
                            className="form-control-sm"
                            type="text"
                            name="name"
                            placeholder="name"
                            required
                            value={this.state.name}
                            onChange={this.handleChangeName}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div className="">
                        <label>Address:<br/>
                            <input
                            className="form-control-sm"
                            type="text"
                            name="address"
                            placeholder="address"
                            required
                            value={this.state.address}
                            onChange={this.handleChangeAddress}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div className="">
                        <label>INN:<br/>
                            <input
                            className="form-control-sm"
                            type="number"
                            name="INN"
                            placeholder="INN"
                            required
                            value={this.state.INN}
                            onChange={this.handleChangeINN}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div>
                        <input
                            className="btn btn-outline-primary btn-sm"
                            type="submit"
                            value="Edit"
                            disabled={!this.state.edited}
                        />
                    </div>
                </form>:
            (type === ModalEssence.DIVISION)?
                <form onSubmit={this.handleSubmitForm}>
                    <div className="">
                        <label>Name:<br/>
                            <input
                            className="form-control-sm"
                            type="text"
                            name="name"
                            placeholder="name"
                            required
                            value={this.state.name}
                            onChange={this.handleChangeName}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div className="">
                        <label>Phone:<br/>
                            <input
                            className="form-control-sm"
                            type="number"
                            name="phone"
                            placeholder="phone"
                            required
                            value={this.state.phone}
                            onChange={this.handleChangePhone}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div>
                        <input
                            className="btn btn-outline-primary btn-sm"
                            type="submit"
                            value="Edit"
                            disabled={!this.state.edited}
                        />
                    </div>
                </form>:
            (type === ModalEssence.EMPLOYEE)?
                <form onSubmit={this.handleSubmitForm}>
                    <div className="">
                        <label>FIO:<br/>
                            <input
                            className="form-control-sm"
                            type="text"
                            name="FIO"
                            placeholder="FIO"
                            required
                            value={this.state.FIO}
                            onChange={this.handleChangeFIO}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div className="">
                        <label>Address:<br/>
                            <input
                            className="form-control-sm"
                            type="text"
                            name="address"
                            placeholder="address"
                            required
                            value={this.state.address}
                            onChange={this.handleChangeAddress}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div className="">
                        <label>Position:<br/>
                            <input
                            className="form-control-sm"
                            type="text"
                            name="position"
                            placeholder="position"
                            required
                            value={this.state.position}
                            onChange={this.handleChangePosition}
                            />
                        </label>
                        { this.state.showError ?
                            <div className="">must be not empty</div>:''
                        }
                    </div>
                    <div>
                        <input
                            className="btn btn-outline-primary btn-sm"
                            type="submit"
                            value="Edit"
                            disabled={!this.state.edited}
                        />
                    </div>
                </form>:
                'wrong type'
          }
      </div>
    );
  }
}
