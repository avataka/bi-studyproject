import * as React from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IActionType } from '../../common'
import { Actions } from '../../Actions/Actions'
import { IDivisionData, IEmployeeData, IOrganizationData } from '../../Actions/Models'
import { IStoreState } from '../../Reducers/Reducers'

import { ModalEssence } from '../../Actions/Consts'
import ModalAdd from './ModalAdd'
import ModalDelete from './ModalDelete'
import ModalEdit from './ModalEdit'

import './Modal.less'

/**
 * Пропсы компонента из стора.
 * @prop {boolean} isOpen Открыто ли окно.
 * @prop {boolean} isOrganization Передана организация.
 * @prop {boolean} isDivision Передано подразделение.
 * @prop {boolean} isEmployee Передан персонал.
 * @prop {boolean} isAdd Окно добавления новой еденицы сущности.
 * @prop {boolean} isEdit Окно редактирования.
 * @prop {boolean} isDelete Окно Удаления.
 * @prop {IOrganizationData} organization Данные переданной организации.
 * @prop {IDivisionData} division Данные переданного подразделения.
 * @prop {IEmployeeData} employee Данные переданного персонала.
 */
interface IStateProps{
    isOpen: boolean
    isOrganization?: boolean
    isDivision?: boolean
    isEmployee?: boolean
    isAdd?: boolean
    isEdit?: boolean
    isDelete?: boolean
    organization?: IOrganizationData
    division?: IDivisionData
    employee?: IEmployeeData
}

/**
* Пропсы для передачи экшенов.
* @prop {Actions} actions Экшены для работы приложения.
*/
export interface IDispatchProps{
  actions: Actions;
}

type TProps = IStateProps & IDispatchProps;

/**
 * @class
 * @name Modal
 * @classdesc Объединяет в себе несколько специализированных.
 */
class Modal extends React.Component<TProps, {}> {

    onClose = () => this.props.actions.onModalClose();

  render() {
    const { isOpen, isOrganization, isDivision, isEmployee, isAdd, isEdit, isDelete, organization, division, employee } = this.props;

    if (isOpen) {
        return <div className="bi-modal__background">
            <div className="bi-modal">
                <input
                    className="bi-modal__close"
                    type="button"
                    value="✖"
                    onClick={this.onClose}
                />

                {
                    isAdd?
                        isOrganization?
                            <ModalAdd action={this.props.actions.onOrganizationAdd.bind(this)} onClose={this.onClose} type={ModalEssence.ORGANIZATION} />
                        :isDivision?
                            <ModalAdd action={this.props.actions.onDivisionAdd.bind(this)} onClose={this.onClose} type={ModalEssence.DIVISION} data={division}/>
                        :isEmployee?
                            <ModalAdd action={this.props.actions.onEmployeeAdd.bind(this)} onClose={this.onClose} type={ModalEssence.EMPLOYEE} data={employee}/>
                        :'wronge essence of modal'
                    :isEdit?
                        isOrganization?
                            <ModalEdit action={this.props.actions.onOrganizationEdit.bind(this)} onClose={this.onClose} type={ModalEssence.ORGANIZATION} data={organization} />
                        :isDivision?
                            <ModalEdit action={this.props.actions.onDivisionEdit.bind(this)} onClose={this.onClose} type={ModalEssence.DIVISION} data={division} />
                        :isEmployee?
                            <ModalEdit action={this.props.actions.onEmployeeEdit.bind(this)} onClose={this.onClose} type={ModalEssence.EMPLOYEE} data={employee} />
                        :'wronge essence of modal'
                    :isDelete?
                        isOrganization?
                            <ModalDelete action={this.props.actions.onOrganizationDelete.bind(this)} onClose={this.onClose} type={ModalEssence.ORGANIZATION} data={organization} />
                        :isDivision?
                            <ModalDelete action={this.props.actions.onDivisionDelete.bind(this)} onClose={this.onClose} type={ModalEssence.DIVISION} data={division} />
                        :isEmployee?
                            <ModalDelete action={this.props.actions.onEmployeeDelete.bind(this)} onClose={this.onClose} type={ModalEssence.EMPLOYEE} data={employee} />
                        :'wronge essence'
                    :'wronge type of modal'
                }

            </div>
        </div>
    } else {
        return <React.Fragment></React.Fragment>
    }
  }
}

const connectModal = connect(
  (state: IStoreState): IStateProps => ({
        isOpen: state.Modal.isOpen,
        isOrganization: state.Modal.isOrganization,
        isDivision: state.Modal.isDivision,
        isEmployee: state.Modal.isEmployee,
        isAdd: state.Modal.isAdd,
        isEdit: state.Modal.isEdit,
        isDelete: state.Modal.isDelete,
        organization: state.Modal.organization,
        division: state.Modal.division,
        employee: state.Modal.employee,
  }),
  (dispatch: Dispatch<IActionType>): IDispatchProps => ({
      actions: new Actions(dispatch)
  })
)(Modal);

export {connectModal as Modal};
