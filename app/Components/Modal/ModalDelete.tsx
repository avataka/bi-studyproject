import * as React from 'react'
import { ModalEssence } from '../../Actions/Consts'

/**
 * @interface
 * @description Пропсы
 */
interface IProps {
    type: ModalEssence,
    action: ({}) => void,
    onClose: () => void,
    data: any/*IOrganizationData|IDivisionData|IEmployeeData*/
}

/**
 * @interface
 * @description Локальное хранилище.
 */
interface IState {
    name: string,
    address: string,
    INN: number,
    phone: number,
    FIO: string,
    position: string,
    checked: boolean
}

/**
 * @class
 * @name ModalDelete
 * @classdesc Тело окна удаления.
 */
export default class ModalDelete extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
          name: typeof this.props.data.name !== 'undefined' ?  this.props.data.name : '',
          address: typeof this.props.data.address !== 'undefined' ?  this.props.data.address : '',
          INN: typeof this.props.data.INN !== 'undefined' ?  this.props.data.INN : 0,
          phone: typeof this.props.data.phone !== 'undefined' ?  this.props.data.phone : 0,
          FIO: typeof this.props.data.FIO !== 'undefined' ?  this.props.data.FIO : '',
          position: typeof this.props.data.position !== 'undefined' ?  this.props.data.position : '',
          checked: false,
        };
    }

    handleSubmitForm = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (this.props.type === ModalEssence.ORGANIZATION) {
            if (this.state.checked) {
                this.props.action({id: this.props.data.id})
                this.props.onClose()
            }
        } else if (this.props.type === ModalEssence.DIVISION) {
            if (this.state.checked) {
                this.props.action({id: this.props.data.id, id_organization: this.props.data.id_organization})
                this.props.onClose()
            }
        } else if (this.props.type === ModalEssence.EMPLOYEE) {
            if (this.state.checked) {
                this.props.action({id: this.props.data.id, id_division: this.props.data.id_division})
                this.props.onClose()
            }
        } else throw 'error'
    };

    handlechecked = () => {
        this.setState({checked: !this.state.checked})
    };

  render() {
    const { type } = this.props;

    return (
      <div>
          {
            (type === ModalEssence.ORGANIZATION)?
                <form onSubmit={this.handleSubmitForm}>
                    <div className="">
                        Do you realy want to delete this organization?
                    </div>
                    <br/>
                    <ul>
                        <li>Name: {this.state.name}</li>
                        <li>Address: {this.state.address}</li>
                        <li>INN: {this.state.INN}</li>
                    </ul>
                    <br/>
                    <div className="">
                        <label>!!! Yes !!!<br/>
                            <input
                            type="checkbox"
                            required
                            onClick={this.handlechecked}
                            defaultChecked={this.state.checked}
                            />
                        </label>
                    </div>
                    <div>
                        <input
                            className="btn btn-outline-primary btn-sm"
                            type="submit"
                            value="Delete"
                            disabled={!this.state.checked}
                        />
                    </div>
                </form>:
            (type === ModalEssence.DIVISION)?
                <form onSubmit={this.handleSubmitForm}>
                    <div className="">
                        Do you realy want to delete this division?
                    </div>
                    <br/>
                    <ul>
                        <li>Name: {this.state.name}</li>
                        <li>Phone: {this.state.phone}</li>
                    </ul>
                    <br/>
                    <div className="">
                        <label>!!! Yes !!!<br/>
                            <input
                            type="checkbox"
                            required
                            onClick={this.handlechecked}
                            defaultChecked={this.state.checked}
                            />
                        </label>
                    </div>
                    <div>
                        <input
                            className="btn btn-outline-primary btn-sm"
                            type="submit"
                            value="Delete"
                            disabled={!this.state.checked}
                        />
                    </div>
                </form>:
            (type === ModalEssence.EMPLOYEE)?
                <form onSubmit={this.handleSubmitForm}>
                    <div className="">
                        Do you realy want to delete this employee?
                    </div>
                    <br/>
                    <ul>
                        <li>FIO: {this.state.FIO}</li>
                        <li>Address: {this.state.address}</li>
                        <li>Position: {this.state.position}</li>
                    </ul>
                    <br/>
                    <div className="">
                        <label>!!! Yes !!!<br/>
                            <input
                            type="checkbox"
                            required
                            onClick={this.handlechecked}
                            defaultChecked={this.state.checked}
                            />
                        </label>
                    </div>
                    <div>
                        <input
                            className="btn btn-outline-primary btn-sm"
                            type="submit"
                            value="Delete"
                            disabled={!this.state.checked}
                        />
                    </div>
                </form>:
                'wrong type'
          }
      </div>
    );
  }
}
