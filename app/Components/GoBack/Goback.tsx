import { History } from 'history';
import * as React from 'react';
import { withRouter } from 'react-router-dom';

/**
 * Кнопка возвращения назад.
 * @param {history: History}
 */
const GoBack = ({ history }: {history: History}) => <input type="button"
                                                       onClick={() => history.goBack()}
                                                       alt="Go back"
                                                       value="🡄 GO BACK"
                                                       className="btn btn-secondary btn-sm"
                                                       />;

export default withRouter(GoBack)
