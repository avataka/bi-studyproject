import * as React from 'react'

import { IEmployeeData } from '../../Actions/Models'
import { IEmployee } from './../../Reducers/EmployeeReducer'
import './EmployeeItem.less'

/**
 * Пропсы компонента из стора.
 * @prop {IEmployee} employee Данные о персонале.
 * @prop {function} onEdit Запрос на редактирование.
 * @prop {function} onDelete Запрос на удаление.
 */
interface IProps {
  employee: IEmployee,
  onEdit: (data: IEmployeeData) => void
  onDelete: () => void
}

/**
 * @class
 * @name EmployeeItem
 * @classdesc Конкретная еденица персонала в списке персонала.
 */
class EmployeeItem extends React.Component<IProps, {}> {

  onEdit = () => {this.props.onEdit({
      id: this.props.employee.id,
      id_division: this.props.employee.id_division,
      FIO: this.props.employee.FIO,
      address: this.props.employee.address,
      position: this.props.employee.position
  })}

  onDelete = () => {this.props.onDelete()}

  render() {
    const {employee} = this.props;

    return <li className="employeeItem">
      <div className="collumn">
        <div>id: {employee.id}</div>
        <div>id_division: {employee.id_division}</div>
        <div>FIO: {employee.FIO}</div>
        <div>address: {employee.address}</div>
        <div>position: {employee.position}</div>
      </div>
      <input
        className="btn btn-outline-primary btn-warning btn-sm"
        type="button"
        value="Edit"
        onClick={this.onEdit}
      />
      <input
        className="btn btn-outline-primary btn-warning btn-sm"
        type="button"
        value="Delete"
        onClick={this.onDelete}
      />
    </li>
  }
}

export default EmployeeItem;
