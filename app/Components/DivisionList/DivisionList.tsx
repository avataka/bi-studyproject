import * as React from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IActionType } from '../../common'
import { Actions } from '../../Actions/Actions'
import { ModalEssence, ModalTypes } from './../../Actions/Consts'

import { IDivision } from './../../Reducers/DivisionReducer'

import DivisionItem from './../DivisionItem/DivisionItem'

/**
* Пропсы для передачи экшенов.
* @prop {Actions} actions Экшены для работы приложения.
*/
export interface IDispatchProps{
  actions: Actions;
}

/**
 * Пропсы компонента.
 * @prop {IDivision[]} divisions Массив подразделений.
 * @prop {string} organization Название организации из роутера.
 */
interface IProps {
  divisions: IDivision[]
  organization: string
}

type TProps = IDispatchProps & IProps;

/**
 * @class
 * @name DivisionList
 * @classdesc Список подразделений.
 */
class DivisionList extends React.Component<TProps, {}> {

  render() {
    const {divisions} = this.props;

    return <React.Fragment>
      <ul>
        {divisions.map((division, index) => <DivisionItem division={division}
                                                            onOpen={this.props.actions.onEmployeeSetId.bind(this, division.id)}
                                                            onEdit={this.props.actions.onModalOpen.bind(this, ModalTypes.EDIT, ModalEssence.DIVISION)}
                                                            onDelete={this.props.actions.onModalOpen.bind(this, ModalTypes.DELETE, ModalEssence.DIVISION, division)}
                                                            organization={this.props.organization}
                                                            key={index}/>)}
      </ul>
    </React.Fragment>
  }
}

const connectDivisionList = connect(
  () => ({}),
  (dispatch: Dispatch<IActionType>): IDispatchProps => ({
      actions: new Actions(dispatch)
  })
)(DivisionList);

export {connectDivisionList as DivisionList};
