import { IActionType } from '../common'
import { ActionTypes, ModalEssence, ModalTypes } from '../Actions/Consts'
import { IDivisionData, IEmployeeData, IOrganizationData } from './../Actions/Models'

/**
 * Пропсы компонента из стора.
 * @prop {boolean} isOpen Открыто ли окно.
 * @prop {boolean} isOrganization Передана организация.
 * @prop {boolean} isDivision Передано подразделение.
 * @prop {boolean} isEmployee Передан персонал.
 * @prop {boolean} isAdd Окно добавления новой еденицы сущности.
 * @prop {boolean} isEdit Окно редактирования.
 * @prop {boolean} isDelete Окно Удаления.
 * @prop {IOrganizationData} organization Данные переданной организации.
 * @prop {IDivisionData} division Данные переданного подразделения.
 * @prop {IEmployeeData} employee Данные переданного персонала.
 */
export interface IModalReducerState{
    isOpen: boolean
    isOrganization: boolean
    isDivision: boolean
    isEmployee: boolean
    isAdd: boolean
    isEdit: boolean
    isDelete: boolean
    organization: IOrganizationData
    division: IDivisionData
    employee: IEmployeeData
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IModalReducerState {
        return {
            isOpen: false,
            isOrganization: false,
            isDivision: false,
            isEmployee: false,
            isAdd: false,
            isEdit: false,
            isDelete: false,
            organization: {
                id: 0,
                name: '',
                address: '',
                INN: 0
            },
            division: {
                id: 0,
                id_organization: 0,
                name: '',
                phone: 0
            },
            employee: {
                id: 0,
                id_division: 0,
                FIO: '',
                address: '',
                position: ''
            }
        };
    }
};

/**
 * @description Хранилище модального окна, хранит полное состояние и данные текущего окна.
 * @param {IDivisionsReducerState} state
 * @param {IActionType} action
 */
export default function ModalReduser(state: IModalReducerState = initialState.state, action: IActionType) {
    switch (action.type) {

        case `${ActionTypes.MODAL_OPEN}${ModalTypes.ADD}${ModalEssence.ORGANIZATION}`:
            return {
                ...state,
                isOpen: true,
                isOrganization: true,
                isAdd: true,
            };

        case `${ActionTypes.MODAL_OPEN}${ModalTypes.EDIT}${ModalEssence.ORGANIZATION}`:
            return {
                ...state,
                isOpen: true,
                isOrganization: true,
                isEdit: true,
                organization: action.payload,
            };

        case `${ActionTypes.MODAL_OPEN}${ModalTypes.DELETE}${ModalEssence.ORGANIZATION}`:
            return {
                ...state,
                isOpen: true,
                isOrganization: true,
                isDelete: true,
                organization: action.payload,
            };

        case `${ActionTypes.MODAL_OPEN}${ModalTypes.ADD}${ModalEssence.DIVISION}`:
            return {
                ...state,
                isOpen: true,
                isDivision: true,
                isAdd: true,
                division: {
                    id: 0,
                    id_organization: action.payload.id_organization,
                    name: '',
                    phone: 0
                }
            };

        case `${ActionTypes.MODAL_OPEN}${ModalTypes.EDIT}${ModalEssence.DIVISION}`:
            return {
                ...state,
                isOpen: true,
                isDivision: true,
                isEdit: true,
                division: action.payload,
            };

        case `${ActionTypes.MODAL_OPEN}${ModalTypes.DELETE}${ModalEssence.DIVISION}`:
            return {
                ...state,
                isOpen: true,
                isDivision: true,
                isDelete: true,
                division: action.payload,
            };

        case `${ActionTypes.MODAL_OPEN}${ModalTypes.ADD}${ModalEssence.EMPLOYEE}`:
            return {
                ...state,
                isOpen: true,
                isEmployee: true,
                isAdd: true,
                employee: {
                    id: 0,
                    id_division: action.payload.id_division,
                    FIO: '',
                    address: '',
                    position: ''
                }
            };

        case `${ActionTypes.MODAL_OPEN}${ModalTypes.EDIT}${ModalEssence.EMPLOYEE}`:
            return {
                ...state,
                isOpen: true,
                isEmployee: true,
                isEdit: true,
                employee: action.payload,
            };

        case `${ActionTypes.MODAL_OPEN}${ModalTypes.DELETE}${ModalEssence.EMPLOYEE}`:
            return {
                ...state,
                isOpen: true,
                isEmployee: true,
                isDelete: true,
                employee: action.payload,
            };

        case `${ActionTypes.MODAL_CLOSE}`:
            return {
                ...state,
                isOpen: false,
                isOrganization: false,
                isDivision: false,
                isEmployee: false,
                isAdd: false,
                isEdit: false,
                isDelete: false,
                organization: {
                    id: 0,
                    name: '',
                    address: '',
                    INN: 0
                },
                division: {
                    id: 0,
                    id_organization: 0,
                    name: '',
                    phone: 0
                },
                employee: {
                    id: 0,
                    id_division: 0,
                    FIO: '',
                    address: '',
                    position: ''
                }
            };
    }
    return state;
}
