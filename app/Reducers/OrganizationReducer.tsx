import {IActionType} from '../common'
import {ActionTypes, AsyncActionTypes} from '../Actions/Consts'

/**
 * Интерфейс организации
 * @prop {number} id Идентификатор организации.
 * @prop {string} name Имя организации.
 * @prop {string} address Адрес организации.
 * @prop {number} INN ИНН организации.
 */
export interface IOrganization{
    id: number
    name: string
    address: string
    INN: number
}

/**
 * Состояние для Organizations хранилища.
 * @prop {IOrganization[]} Organizations Все организации.
 * @prop {boolean} isLoading Ожидание завершения процедуры получения данных.
 * @prop {boolean} isError Попытка получения данных провалена.
 */
export interface IOrganizationsReducerState{
    organizations: IOrganization[]
    isLoading: boolean
    isError: boolean
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IOrganizationsReducerState {
        return {
            organizations: [],
            isLoading: false,
            isError: false
        };
    }
};

/**
 * @description Хранилище организаций.
 * @param {IDivisionsReducerState} state
 * @param {IActionType} action
 */
export default function OrganizationReduser(state: IOrganizationsReducerState = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.ORGANIZATION_GET_ALL}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                isLoading: true,
            };

        case `${ActionTypes.ORGANIZATION_GET_ALL}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                organizations: action.payload,
                isLoading: false,
                isError: false,
            };

        case `${ActionTypes.ORGANIZATION_GET_ALL}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                isLoading: false,
                organizations: [],
                isError: true,
            };
    }
    return state;
}
