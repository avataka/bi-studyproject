import { connectRouter } from 'connected-react-router'
import { combineReducers } from 'redux'

import { History } from 'history';
import DivisionReducer, { IDivisionsReducerState } from './DivisionReducer'
import EmployeeReducer, { IEmployeesReducerState } from './EmployeeReducer'
import LoginReducer, { ILoginReducerState } from './LoginReducer'
import ModalReducer, { IModalReducerState } from './ModalReducer'
import OrganizationReducer, { IOrganizationsReducerState } from './OrganizationReducer'

/**
 * @interface
 * @description интерфейс главного хранилища.
 */
export interface IStoreState {
    router: any
    LoginReducer: ILoginReducerState
    Organization: IOrganizationsReducerState
    Division: IDivisionsReducerState
    Modal: IModalReducerState
    Employee: IEmployeesReducerState
}

/**
 * Возвращает редьюсер.
 * @param {History} history
 */
const createRootReducer = (history: History) => combineReducers<IStoreState>({
    router: connectRouter(history),
    LoginReducer: LoginReducer,
    Organization: OrganizationReducer,
    Division: DivisionReducer,
    Modal: ModalReducer,
    Employee: EmployeeReducer,

})

export default createRootReducer
