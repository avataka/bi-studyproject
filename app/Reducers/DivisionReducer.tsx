import {IActionType} from '../common'
import {ActionTypes, AsyncActionTypes} from '../Actions/Consts'

/**
 * Интерфейс организации
 * @prop {number} id Идентификатор подразделения.
 * @prop {number} id_organization Идентификатор организации.
 * @prop {string} name Название подразделения.
 * @prop {number} phone Номер телефона.
 */
export interface IDivision{
    id: number
    id_organization: number
    name: string
    phone: number
}

/**
 * Состояние для Organizations хранилища.
 * @prop {IDivision[]} divisions Все подразделения в организации.
 * @prop {boolean} isLoading Ожидание завершения процедуры получения данных.
 * @prop {boolean} isError Попытка получения данных провалена.
 * @prop {number} id_organization Идентификатор текущей организации. Необходим для запроса подразделений после смены страницы.
 */
export interface IDivisionsReducerState{
    divisions: IDivision[]
    isLoading: boolean
    isError: boolean
    id_organization: number
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IDivisionsReducerState {
        return {
            divisions: [],
            isLoading: false,
            isError: false,
            id_organization: 0
        };
    }
};

/**
 * @description Хранилище подразделений.
 * @param {IDivisionsReducerState} state
 * @param {IActionType} action
 */
export default function DivisionReduser(state: IDivisionsReducerState = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.DIVISION_GET_ALL}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                isLoading: true,
            };

        case `${ActionTypes.DIVISION_GET_ALL}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                divisions: action.payload,
                isLoading: false,
                isError: false,
            };

        case `${ActionTypes.DIVISION_GET_ALL}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                isLoading: false,
                divisions: [],
                isError: true,
            };

        case `${ActionTypes.DIVISION_SET_ID}`:
            return {
                ...state,
                id_organization: action.payload,
            };
    }
    return state;
}
