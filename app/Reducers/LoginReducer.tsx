import {IActionType} from '../common'
import {ActionTypes, AsyncActionTypes} from '../Actions/Consts'

/**
 * Состояние для Login хранилища.
 * @prop {boolean} loginStatus Состояние зарегистрированности пользователя.
 * @prop {boolean} loading Ожидание завершения процедуры авторизации (завершение логина).
 * @prop {boolean} isLoginWrong Попытка авторизации провалена
 */
export interface ILoginReducerState{
    loginStatus: boolean
    loading: boolean
    isLoginWrong: boolean
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): ILoginReducerState {
        return {
            loginStatus: false,
            loading: false,
            isLoginWrong: false
        };
    }
};

/**
 * @description Хранилище авторизации пользователя.
 * @param {IDivisionsReducerState} state
 * @param {IActionType} action
 */
export default function BaseReduser(state: ILoginReducerState = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                loading: true,
            };

        case `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                loginStatus: true,
                loading: false,
                isLoginWrong: false,
            };

        case `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                loading: false,
                loginStatus: false,
                isLoginWrong: true,
            };

        case ActionTypes.LOGOUT:
            return {
                ...state,
                loginStatus: false,
            };

        case ActionTypes.LOGIN_WRONG:
            return {
                ...state,
                isLoginWrong: true,
            };
    }
    return state;
}
