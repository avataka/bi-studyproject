import {IActionType} from '../common'
import {ActionTypes, AsyncActionTypes} from '../Actions/Consts'

/**
 * Интерфейс персонала
 * @prop {number} id Идентификатор персонала.
 * @prop {number} id_division Идентификатор подразделения.
 * @prop {string} FIO ФИО персонала.
 * @prop {string} address Адресс персонала.
 * @prop {string} position Должность персонала.
 */
export interface IEmployee{
    id: number
    id_division: number
    FIO: string
    address: string
    position: string
}

/**
 * Состояние для employees хранилища.
 * @prop {IEmployee[]} employees Весь персонал в подразделении.
 * @prop {boolean} isLoading Ожидание завершения процедуры получения данных.
 * @prop {boolean} isError Попытка получения данных провалена.
 * @prop {number} id_division Идентификатор текущего подразделения. Необходим для запроса персонала после смены страницы.
 */
export interface IEmployeesReducerState{
    employees: IEmployee[]
    isLoading: boolean
    isError: boolean
    id_division: number
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IEmployeesReducerState {
        return {
            employees: [],
            isLoading: false,
            isError: false,
            id_division: 0
        };
    }
};

/**
 * @description Хранилище персонала.
 * @param {IDivisionsReducerState} state
 * @param {IActionType} action
 */
export default function DivisionReduser(state: IEmployeesReducerState = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.EMPLOYEE_GET_ALL}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                isLoading: true,
            };

        case `${ActionTypes.EMPLOYEE_GET_ALL}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                employees: action.payload,
                isLoading: false,
                isError: false,
            };

        case `${ActionTypes.EMPLOYEE_GET_ALL}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                isLoading: false,
                employees: [],
                isError: true,
            };

        case `${ActionTypes.EMPLOYEE_SET_ID}`:
            return {
                ...state,
                id_division: action.payload,
            };
    }
    return state;
}
