import * as React from 'react'

import './PublicPage.less'

/**
 * Главная страница до успешной авторизации.
 */
export default class PublicPage extends React.Component<{}, {}> {

  render() {
    return (
      <div className="PublicPage">
        <h3>
          Главная страница до прохождения авторизации
        </h3>
        <p>Вы всегда со всех адресов будете попадать на этот адресс, пока не авторизуетесь</p>
      </div>
    );
  }
}
