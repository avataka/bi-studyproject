import * as React from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IActionType } from '../../common'
import { Actions } from '../../Actions/Actions'
import { IStoreState } from '../../Reducers/Reducers'

import { DivisionList } from '../../Components/DivisionList/DivisionList';
import { ModalEssence, ModalTypes } from './../../Actions/Consts'
import GoBack from './../../Components/GoBack/Goback'
import { IDivision } from './../../Reducers/DivisionReducer'

import './DivisionsPage.less'

/**
 * Пропсы компонента из стора.
 */
interface IStateProps {
    divisions: IDivision[]
    isLoading: boolean
    isError: boolean
    idOrganization: number
}

interface IProps {
    match: any
    location: any
}

/**
* Пропсы для передачи экшенов.
* @prop {Actions} actions Экшены для работы приложения.
*/
export interface IDispatchProps{
  actions: Actions;
}

type TProps = IStateProps & IDispatchProps & IProps /*& IOrganizationsReducerState*/;

/**
 * @class
 * @name DivisionsPage
 * @classdesc Страница всех подразделений.
 */
class DivisionsPage extends React.Component<TProps, {}> {

  componentDidMount() {
    this.props.actions.onDivisionGet(this.props.idOrganization);
  }

  handleAdd = () => this.props.actions.onModalOpen( ModalTypes.ADD, ModalEssence.DIVISION, {id_organization: this.props.idOrganization})

  render() {
    const {divisions, isLoading, isError} = this.props;

    return (
      <div className="DivisionPage">
        <GoBack/>
        <h3>
          Divisions of {this.props.match.params.organization.replace(/-/g, ' ')}
        </h3>
        <input
            className="btn btn-primary btn-sm"
            disabled={isLoading}
            type="button"
            value="Add division"
            onClick={this.handleAdd}
        />
        {
          isLoading?
          <div>Загрузка...</div>:
          isError?
          <div>Ошибка соединения или ответа</div>:''
        }
        {
          divisions.length === 0 ?
          <h4>Подразделений нет</h4>:
            <DivisionList divisions={divisions} organization={this.props.match.params.organization}/>
        }
      </div>
    );
  }
}

const connectDivisionsPage = connect(
  (state: IStoreState): IStateProps => ({
      divisions: state.Division.divisions,
      isLoading: state.Division.isLoading,
      isError: state.Division.isError,
      idOrganization: state.Division.id_organization,
  }),
  (dispatch: Dispatch<IActionType>): IDispatchProps => ({
      actions: new Actions(dispatch)
  })
)(DivisionsPage);

export {connectDivisionsPage as DivisionsPage};
