import * as React from 'react'
import { Route, Switch} from 'react-router-dom'

import { DivisionsPage } from '../DivisionsPage/DivisionsPage'
import { EmployeesPage } from '../EmployeesPage/EmployeesPage'
import MainPage from '../MainPage/MainPage'
import { OrganizationsPage } from '../OrganizationsPage/OrganizationsPage'

/**
 * Путь роутинга, недоступный до успешной авторизации.
 */
class PrivateWay extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route path="/organizations/:organization/:division" component={EmployeesPage}/>
          <Route path="/organizations/:organization" component={DivisionsPage}/>
          <Route path="/organizations" component={OrganizationsPage}/>
          <Route path="/" component={MainPage}/>
        </Switch>
      </React.Fragment>
    )
  }
}

export default PrivateWay
