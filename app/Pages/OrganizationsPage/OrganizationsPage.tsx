import * as React from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IActionType } from '../../common'
import { Actions } from '../../Actions/Actions'
import { IStoreState } from '../../Reducers/Reducers'

import { OrganizationList } from '../../Components/OrganizationList/OrganizationList'
import { ModalEssence, ModalTypes } from './../../Actions/Consts'
import GoBack from './../../Components/GoBack/Goback'
import { IOrganizationsReducerState } from './../../Reducers/OrganizationReducer'

import './OrganizationsPage.less'

/**
* Пропсы для передачи экшенов.
* @prop {Actions} actions Экшены для работы приложения.
*/
export interface IDispatchProps{
  actions: Actions;
}

type TProps = IDispatchProps & IOrganizationsReducerState;

/**
 * @class
 * @name OrganizationsPage
 * @classdesc Страница всех организаций.
 */
class OrganizationsPage extends React.Component<TProps, {}> {

  componentDidMount() {
    this.props.actions.onOrganizationGet();
  }

  handleAdd = () => this.props.actions.onModalOpen( ModalTypes.ADD, ModalEssence.ORGANIZATION )

  render() {
    const {organizations, isLoading, isError} = this.props;

    return (
      <div className="OrganizationPage">
        <GoBack/>
        <h3>
          All Organizations
        </h3>
        <input
          className="btn btn-primary btn-sm"
          disabled={isLoading}
          type="button"
          value="Add organization"
          onClick={this.handleAdd}
        />
        {
          isLoading?
          <div>Загрузка...</div>:
          isError?
          <div>Ошибка соединения или ответа</div>:''
        }
        {
          organizations.length === 0 ?
          <h4>Организаций нет</h4>:
          <OrganizationList organizations={organizations} />
        }
      </div>
    );
  }
}

const connectOrganizationsPage = connect(
  (state: IStoreState): IOrganizationsReducerState => ({
      organizations: state.Organization.organizations,
      isLoading: state.Organization.isLoading,
      isError: state.Organization.isError
  }),
  (dispatch: Dispatch<IActionType>): IDispatchProps => ({
      actions: new Actions(dispatch)
  })
)(OrganizationsPage);

export {connectOrganizationsPage as OrganizationsPage};
