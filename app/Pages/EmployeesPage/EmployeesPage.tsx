import * as React from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IActionType } from '../../common'
import { Actions } from '../../Actions/Actions'
import { IStoreState } from '../../Reducers/Reducers'

import { EmployeeList } from '../../Components/EmployeeList/EmployeeList';
import { ModalEssence, ModalTypes } from './../../Actions/Consts'
import GoBack from './../../Components/GoBack/Goback'
import { IEmployee } from './../../Reducers/EmployeeReducer'

import './EmployeesPage.less'

/**
 * Пропсы компонента из стора.
 */
interface IStateProps {
    employees: IEmployee[]
    isLoading: boolean
    isError: boolean
    idDivision: number
}

interface IProps {
    match: any
    location: any
}

/**
* Пропсы для передачи экшенов.
* @prop {Actions} actions Экшены для работы приложения.
*/
export interface IDispatchProps{
  actions: Actions;
}

type TProps = IStateProps & IDispatchProps & IProps /*& IOrganizationsReducerState*/;

/**
 * @class
 * @name EmployeesPage
 * @classdesc Страница всего персонала.
 */
class EmployeesPage extends React.Component<TProps, {}> {

  componentDidMount() {
    this.props.actions.onEmployeeGet(this.props.idDivision);
  }

  handleAdd = () => this.props.actions.onModalOpen( ModalTypes.ADD, ModalEssence.EMPLOYEE, {id_division: this.props.idDivision})

  render() {
    const {employees, isLoading, isError} = this.props;

    return (
      <div className="EmployeePage">
        <GoBack/>
        <h3>
            Employees of {this.props.match.params.division.replace(/-/g, ' ')}
        </h3>
        <input
            className="btn btn-primary btn-sm"
            disabled={isLoading}
            type="button"
            value="Add employee"
            onClick={this.handleAdd}
        />
        {
          isLoading?
          <div>Загрузка...</div>:
          isError?
          <div>Ошибка соединения или ответа</div>:''
        }
        {
          employees.length === 0 ?
          <h4>Персонала нет</h4>:
            <EmployeeList employees={employees} />
        }
      </div>
    );
  }
}

const connectEmployeesPage = connect(
  (state: IStoreState): IStateProps => ({
      employees: state.Employee.employees,
      isLoading: state.Employee.isLoading,
      isError: state.Employee.isError,
      idDivision: state.Employee.id_division,
  }),
  (dispatch: Dispatch<IActionType>): IDispatchProps => ({
      actions: new Actions(dispatch)
  })
)(EmployeesPage);

export {connectEmployeesPage as EmployeesPage};
