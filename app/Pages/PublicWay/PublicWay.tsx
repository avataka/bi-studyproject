import * as React from 'react'
import { Redirect } from 'react-router-dom'

import PublicPage from '../PublicPage/PublicPage'

/**
 * Путь роутинга до успешной авторизации.
 */
export default class PublicWay extends React.Component<{}, {}> {
    render() {
        return (
            <React.Fragment>
                <Redirect to="/"/>
                <PublicPage/>
            </React.Fragment>
        )
    }
}
