import * as React from 'react'
import { Link} from 'react-router-dom'

import './MainPage.less'

/**
 * Главная страница после успешной авторизации.
 */
export default class MainPage extends React.Component<{}, {}> {

  render() {
    return (
      <div className="MainPage">
        <h3>
          Главная страница, после прохождения авторизации
        </h3>
        <div>На этой странице может отображаться какая-то служебная информация.</div>
        <Link to="/organizations">Перейти к организациям</Link>
      </div>
    );
  }
}
