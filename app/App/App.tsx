import * as React from 'react'
import { connect } from 'react-redux'
import { Route, Switch } from 'react-router-dom'
import { IStoreState } from '../Reducers/Reducers'

import Footer from '../Components/Footer/Footer'
import Header from '../Components/Header/Header'
import { Modal } from '../Components/Modal/Modal'
import PrivateWay from '../Pages/PrivateWay/PrivateWay'
import PublicWay from '../Pages/PublicWay/PublicWay'

import './App.less'

/**
 * Пропсы компонента из стора.
 * @prop {boolean} loginStatus Состояние авторизации пользователя.
 */
interface IStateProps{
    loginStatus: boolean;
}

type TProps = IStateProps;

class App extends React.Component<TProps, {}> {

    render() {
        const {loginStatus} = this.props;

        return (
            <React.Fragment>
                <Modal/>
                <Header/>
                <main className="">
                    <Switch>
                        <Route path="/" component={loginStatus?PrivateWay:PublicWay} />
                    </Switch>
                </main>
                <Footer/>
            </React.Fragment>
        );
    }
}

const connectApp = connect(
    (state: IStoreState): IStateProps => ({
        loginStatus: state.LoginReducer.loginStatus
    }),
    {}
)(App);

export {connectApp as App};
