/**
 * @interface
 * @name ILoginData
 * @description Модель данных для авторизации.
 * @prop {{login: string, password: string}} loginData Объект с полями, указанными ниже. Серверу нужен этот объект в теле запроса.
 * @prop {string} loginData.login Имя пользователя.
 * @prop {string} loginData.password Пароль пользователя.
 */
export interface ILoginData {
    loginData: {
        login: string;
        password: string;
    }
}

/**
 * @interface
 * @name IOrganizationData
 * @description Описание данных организации.
 * @prop {number} id Идентификатор организации.
 * @prop {string} name Название организации.
 * @prop {string} address Адрес организации.
 * @prop {number} INN ИНН организации.
 */
export interface IOrganizationData {
    id?: number
    name: string
    address: string
    INN: number
}

/**
 * @interface
 * @name IDivisionData
 * @description Описание данных поразделения.
 * @prop {number} id Идентификатор организации.
 * @prop {number} id_organization Идентификатор организации.
 * @prop {string} name Название поразделения.
 * @prop {number} phone Телефон поразделения.
 */
export interface IDivisionData {
    id?: number
    id_organization: number
    name: string
    phone: number
}

/**
 * @interface
 * @name IEmployeeData
 * @description Описание данных персонала.
 * @prop {number} id Идентификатор персонала.
 * @prop {number} id_division Идентификатор поразделения.
 * @prop {string} FIO ФИО персонала.
 * @prop {string} address Адрес персонала.
 * @prop {string} position Должность персонала.
 */
export interface IEmployeeData {
    id?: number
    id_division: number
    FIO: string
    address: string
    position: string
}
