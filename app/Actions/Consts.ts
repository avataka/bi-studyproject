/**
 * @enum {string}
 * @readonly
 * @name ActionTypes
 * @description Типы экшенов, используемые в приложении.Константы.
 *
 * @prop {string} LOGIN Авторизация.
 * @prop {string} LOGOUT Отмена авторизации.
 * @prop {string} LOGIN_WRONG Пользователь не найден или пароль не совпадает.
 *
 * @prop {string} ORGANIZATION_GET_ALL Запросить все организации.
 * @prop {string} ORGANIZATION_EDIT Редактировать организацию.
 * @prop {string} ORGANIZATION_DELETE Удалить организацию.
 * @prop {string} ORGANIZATION_ADD Добавить организацию.
 *
 * @prop {string} DIVISION_SET_ID Сохранить идентификатор текущей организации. Необходим при смене страниц.
 * @prop {string} DIVISION_GET_ALL Запросить все подразделения в организации.
 * @prop {string} DIVISION_EDIT Редактировать подразделение.
 * @prop {string} DIVISION_DELETE Удалить подразделение.
 * @prop {string} DIVISION_ADD Добавить подразделение.
 *
 * @prop {string} EMPLOYEE_SET_ID Сохранить идентификатор текущего подразделения. Необходим при смене страниц.
 * @prop {string} EMPLOYEE_GET_ALL Запросить весь персонал в подразделении.
 * @prop {string} EMPLOYEE_EDIT Редактировать персонал.
 * @prop {string} EMPLOYEE_DELETE Удалить персонал.
 * @prop {string} EMPLOYEE_ADD Добавить персонал.
 *
 * @prop {string} MODAL_OPEN Открыть модальное окно.
 * @prop {string} MODAL_CLOSE Закрыть модальное окно.
 */
export enum ActionTypes {
    LOGIN = 'ACTION_LOGIN',
    LOGOUT = 'ACTION_LOGOUT',
    LOGIN_WRONG = 'ACTION_LOGIN_WRONG',
    ORGANIZATION_GET_ALL = 'ORGANIZATION_GET_ALL',
    ORGANIZATION_EDIT = 'ORGANIZATION_EDIT',
    ORGANIZATION_DELETE = 'ORGANIZATION_DELETE',
    ORGANIZATION_ADD = 'ORGANIZATION_ADD',
    DIVISION_SET_ID = 'DIVISION_SET_ID',
    DIVISION_GET_ALL = 'DIVISION_GET_ALL',
    DIVISION_EDIT = 'DIVISION_EDIT',
    DIVISION_DELETE = 'DIVISION_DELETE',
    DIVISION_ADD = 'DIVISION_ADD',
    EMPLOYEE_SET_ID = 'EMPLOYEE_SET_ID',
    EMPLOYEE_GET_ALL = 'EMPLOYEE_GET_ALL',
    EMPLOYEE_EDIT = 'EMPLOYEE_EDIT',
    EMPLOYEE_DELETE = 'EMPLOYEE_DELETE',
    EMPLOYEE_ADD = 'EMPLOYEE_ADD',
    MODAL_OPEN = 'MODAL_OPEN',
    MODAL_CLOSE = 'MODAL_CLOSE',
}

/**
 * @enum {string}
 * @readonly
 * @name AsyncActionTypes
 * @description Подтипы для экшенов при ассинхронной работы.
 * @prop {string} BEGIN Начало ассинхронного действия.
 * @prop {string} SUCCESS Действие завершилось успешно.
 * @prop {string} FAILURE Действие завершилось с ошибкой.
 */
export enum AsyncActionTypes {
    BEGIN = '_BEGIN',
    SUCCESS = '_SUCCESS',
    FAILURE = '_FAILURE',
}

/**
 * @enum {string}
 * @readonly
 * @name ActionTypes
 * @description Типы открываемых модальных окон.
 * @prop {string} ADD Добавление.
 * @prop {string} EDIT Редактирование.
 * @prop {string} DELETE Удаление.
 */
export enum ModalTypes {
    ADD = '_ADD',
    EDIT = '_EDIT',
    DELETE = '_DELETE',
}

/**
 * @enum {string}
 * @readonly
 * @name ActionTypes
 * @description Типы сущностей для модальных окон.
 * @prop {string} ORGANIZATION Организация.
 * @prop {string} DIVISION Подразделение.
 * @prop {string} EMPLOYEE Персонал.
 */
export enum ModalEssence {
    ORGANIZATION = '_ORGANIZATION',
    DIVISION = '_DIVISION',
    EMPLOYEE = '_EMPLOYEE',
}
