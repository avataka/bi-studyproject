import { Dispatch } from 'redux';
import { IActionType } from '../common';
import { ActionTypes, AsyncActionTypes, ModalEssence, ModalTypes } from './Consts';
import { IDivisionData, IEmployeeData, ILoginData, IOrganizationData } from './Models';

/**
 * @class
 * @name Actions
 * @classdesc Класс, методы которого являются создателями экшенов.
 */
export class Actions {

    /**
     * @constructor
     * @param {Dispatch<IActionType>} dispatch
     */
    constructor(private dispatch: Dispatch<IActionType>) {}

    /**
     * @method onLogin Запрос авторизации.
     * @param {ILoginData} loginData Данные для авторизации.
     */
    onLogin = (loginData: ILoginData) => {
        this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`});

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(loginData),
        };
        fetch('http://127.0.0.1:8080/athorize', options)
            .then(response => {
                if (response.status === 200) {
                    return response.json()
                } else {
                    throw 'error';
                }
            })
            .then(data => {
                if (data.isLogin === true) {
                    this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`});
                } else {
                    this.dispatch({type: `${ActionTypes.LOGIN_WRONG}`});
                    throw 'error';
                }
            })
            .catch(error => {
                this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`, payload: error});
            });
    };

    /**
     * @method onLogout Выход пользователя.
     * @todo Апи сервера еще не готово. Не куда обращаться для выхода пользователя.
     */
    onLogout = () => {
        const options = {
            method: 'POST',
        };
        fetch('http://127.0.0.1:8080/logout', options)  //TODO адреса запроса еще не существует.
        .then(response => {
            if (response.status === 200) {
                this.dispatch({type: ActionTypes.LOGOUT});
            } else {
                throw 'error';
            }
        })
        .catch(() => {
            this.dispatch({type: ActionTypes.LOGOUT});
        });
    };

    /**
     * @method onOrganizationGet Запрос всех организаций.
     */
    onOrganizationGet = () => {
        this.dispatch({type: `${ActionTypes.ORGANIZATION_GET_ALL}${AsyncActionTypes.BEGIN}`});

        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        }
        fetch('http://127.0.0.1:8080/organization', options)
          .then(response => {
            if (response.status === 200) {
                return response.json()
            } else {
                throw 'error';
            }
          })
          .then(data => {
            this.dispatch({type: `${ActionTypes.ORGANIZATION_GET_ALL}${AsyncActionTypes.SUCCESS}`, payload: data});
          })
          .catch(error => {
            this.dispatch({type: `${ActionTypes.ORGANIZATION_GET_ALL}${AsyncActionTypes.FAILURE}`, payload: error});
          })
    }

    /**
     * @method onOrganizationEdit Запрос на редактирование данных организации.
     * @param {object} data Данные об организации
     * @param {number} data.id Уникальный идентификатор организации.
     * @param {number} data.name Название организации.
     * @param {number} data.address Адрес организации.
     * @param {number} data.INN ИНН организации.
     */
    onOrganizationEdit = (data: IOrganizationData) => {
        this.dispatch({type: `${ActionTypes.ORGANIZATION_EDIT}${AsyncActionTypes.BEGIN}`});

        const options = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data),
        }
        fetch('http://127.0.0.1:8080/editOrganization', options)
          .then(response => {
            if (response.status === 200) {
                return response.json()
            } else {
                throw 'error';
            }
          })
          .then(answer => {
              if ( answer.success === true ) {
                this.dispatch({type: `${ActionTypes.ORGANIZATION_EDIT}${AsyncActionTypes.SUCCESS}`});
                this.onOrganizationGet();
              } else {
                throw 'error';
              }
          })
          .catch(error => {
            this.dispatch({type: `${ActionTypes.ORGANIZATION_EDIT}${AsyncActionTypes.FAILURE}`, payload: error});
          })
    }

    /**
     * @method onOrganizationDelete Запрос на удаление оганизации.
     * @param {IOrganizationData} data
     * @param {number} data.id Уникальный идентификатор организации. Для удалеения необходим только он.
     */
    onOrganizationDelete = (data: IOrganizationData) => {
        this.dispatch({type: `${ActionTypes.ORGANIZATION_DELETE}${AsyncActionTypes.BEGIN}`});

        const options = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data),
        }
        fetch('http://127.0.0.1:8080/deleteOrganization', options)
          .then(response => {
            if (response.status === 200) {
                return response.json()
            } else {
                throw 'error';
            }
          })
          .then(answer => {
              if ( answer.success === true ) {
                this.dispatch({type: `${ActionTypes.ORGANIZATION_DELETE}${AsyncActionTypes.SUCCESS}`});
                this.onOrganizationGet();
              } else {
                throw 'error';
              }
          })
          .catch(error => {
            this.dispatch({type: `${ActionTypes.ORGANIZATION_DELETE}${AsyncActionTypes.FAILURE}`, payload: error});
          })
    }

    /**
     * @method onOrganizationAdd Запрос на добавление организации.
     * @param {IOrganizationData} data Данные об организации.
     */
    onOrganizationAdd = ( data: IOrganizationData ) => {
        this.dispatch({type: `${ActionTypes.ORGANIZATION_ADD}${AsyncActionTypes.BEGIN}`});

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data),
        }
        fetch('http://127.0.0.1:8080/createOrganization', options)
          .then(response => {
            if (response.status === 200) {
                return response.json()
            } else {
                throw 'error';
            }
          })
          .then(answer => {
              if ( answer.success === true ) {
                this.dispatch({type: `${ActionTypes.ORGANIZATION_ADD}${AsyncActionTypes.SUCCESS}`});
                this.onOrganizationGet();
              } else {
                throw 'error';
              }
          })
          .catch(error => {
            this.dispatch({type: `${ActionTypes.ORGANIZATION_ADD}${AsyncActionTypes.FAILURE}`, payload: error});
          })
    }

    /**
     * @method onDivisionSetId Запись идентификатора открываемой организации. Необходимо при смене страницы.
     * @param {number} id Идентификатор организации.
     */
    onDivisionSetId = (id: number) => {
        this.dispatch({type: `${ActionTypes.DIVISION_SET_ID}`, payload: id});
    }

    /**
     * @method onDivisionGet  Запрос на получение подразделений конкретной организации.
     * @param {number} id Идентификатор организации.
     */
    onDivisionGet = (id: number) => {
        this.dispatch({type: `${ActionTypes.DIVISION_GET_ALL}${AsyncActionTypes.BEGIN}`});
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        }
        fetch(`http://127.0.0.1:8080/division?id=${id}`, options)
          .then(response => {
            if (response.status === 200) {
                return response.json()
            } else {
                throw 'error';
            }
          })
          .then(data => {
            this.dispatch({type: `${ActionTypes.DIVISION_GET_ALL}${AsyncActionTypes.SUCCESS}`, payload: data});
          })
          .catch(error => {
            this.dispatch({type: `${ActionTypes.DIVISION_GET_ALL}${AsyncActionTypes.FAILURE}`, payload: error});
          })
    }

    /**
     * @method onDivisionEdit Запрос на редактирование подразделения.
     * @param {IDivisionData} data Информация о подразделении.
     */
    onDivisionEdit = (data: IDivisionData) => {
        this.dispatch({type: `${ActionTypes.DIVISION_EDIT}${AsyncActionTypes.BEGIN}`});

        const options = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data),
        }
        fetch('http://127.0.0.1:8080/editDivision', options)
          .then(response => {
            if (response.status === 200) {
                return response.json()
            } else {
                throw 'error';
            }
          })
          .then(answer => {
              if ( answer.success === true ) {
                this.dispatch({type: `${ActionTypes.DIVISION_EDIT}${AsyncActionTypes.SUCCESS}`});
                this.onDivisionGet(data.id_organization);
              } else {
                throw 'error';
              }
          })
          .catch(error => {
            this.dispatch({type: `${ActionTypes.DIVISION_EDIT}${AsyncActionTypes.FAILURE}`, payload: error});
          })
    }

    /**
     * @method onDivisionDelete Запрос на удаление подразделения.
     * @param {IDivisionData} data
     * @param {number} data.id Идентификатор удаляемого подразделения.
     */
    onDivisionDelete = (data: IDivisionData) => {
        this.dispatch({type: `${ActionTypes.DIVISION_DELETE}${AsyncActionTypes.BEGIN}`});

        const options = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data),
        }
        fetch('http://127.0.0.1:8080/deleteDivision', options)
          .then(response => {
            if (response.status === 200) {
                return response.json()
            } else {
                throw 'error';
            }
          })
          .then(answer => {
              if ( answer.success === true ) {
                this.dispatch({type: `${ActionTypes.DIVISION_DELETE}${AsyncActionTypes.SUCCESS}`});
                this.onDivisionGet(data.id_organization);
              } else {
                throw 'error';
              }
          })
          .catch(error => {
            this.dispatch({type: `${ActionTypes.DIVISION_DELETE}${AsyncActionTypes.FAILURE}`, payload: error});
          })
    }

    /**
     * @method onDivisionAdd Запрос на добавление подразделения.
     * @param {IDivisionData} data Данные об подразделении.
     */
    onDivisionAdd = (data: IDivisionData) => {
        this.dispatch({type: `${ActionTypes.DIVISION_ADD}${AsyncActionTypes.BEGIN}`});
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data),
        }
        fetch('http://127.0.0.1:8080/createDivision', options)
          .then(response => {
            if (response.status === 200) {
                return response.json()
            } else {
                throw 'error';
            }
          })
          .then(answer => {
              if ( answer.success === true ) {
                this.dispatch({type: `${ActionTypes.DIVISION_ADD}${AsyncActionTypes.SUCCESS}`});
                this.onDivisionGet(data.id_organization);
              } else {
                throw 'error';
              }
          })
          .catch(error => {
            this.dispatch({type: `${ActionTypes.DIVISION_ADD}${AsyncActionTypes.FAILURE}`, payload: error});
          })
    }

    /**
     * @method onEmployeeSetId Запись идентификатора открываемого персонала. Необходимо при смене страницы.
     * @param {number} id Идентификатор подразделения.
     */
    onEmployeeSetId = (id: number) => {
        this.dispatch({type: `${ActionTypes.EMPLOYEE_SET_ID}`, payload: id});
    }

    /**
     * @method onEmployeeGet  Запрос на получение персонала конкретного подразделения.
     * @param {number} id Идентификатор подразделения.
     */
    onEmployeeGet = (id: number) => {
        this.dispatch({type: `${ActionTypes.EMPLOYEE_GET_ALL}${AsyncActionTypes.BEGIN}`});

        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        }
        fetch(`http://127.0.0.1:8080/employee?id=${id}`, options)
          .then(response => {
            if (response.status === 200) {
                return response.json()
            } else {
                throw 'error';
            }
          })
          .then(data => {
            this.dispatch({type: `${ActionTypes.EMPLOYEE_GET_ALL}${AsyncActionTypes.SUCCESS}`, payload: data});
          })
          .catch(error => {
            this.dispatch({type: `${ActionTypes.EMPLOYEE_GET_ALL}${AsyncActionTypes.FAILURE}`, payload: error});
          })
    }

    /**
     * @method onEmployeeEdit Запрос на редактирование персонала.
     * @param {IEmployeeData} data Информация о персонале.
     */
    onEmployeeEdit = (data: IEmployeeData) => {
        this.dispatch({type: `${ActionTypes.EMPLOYEE_EDIT}${AsyncActionTypes.BEGIN}`});

        const options = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data),
        }
        fetch('http://127.0.0.1:8080/editEmployee', options)
          .then(response => {
            if (response.status === 200) {
                return response.json()
            } else {
                throw 'error';
            }
          })
          .then(answer => {
              if ( answer.success === true ) {
                this.dispatch({type: `${ActionTypes.EMPLOYEE_EDIT}${AsyncActionTypes.SUCCESS}`});
                this.onEmployeeGet(data.id_division);
              } else {
                throw 'error';
              }
          })
          .catch(error => {
            this.dispatch({type: `${ActionTypes.EMPLOYEE_EDIT}${AsyncActionTypes.FAILURE}`, payload: error});
          })
    }

    /**
     * @method onEmployeeDelete Запрос на удаление персонала.
     * @param {IEmployeeData} data
     * @param {number} data.id Идентификатор удаляемого персонала.
     */
    onEmployeeDelete = (data: IEmployeeData) => {
        this.dispatch({type: `${ActionTypes.EMPLOYEE_DELETE}${AsyncActionTypes.BEGIN}`});

        const options = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data),
        }
        fetch('http://127.0.0.1:8080/deleteEmployee', options)
          .then(response => {
            if (response.status === 200) {
                return response.json()
            } else {
                throw 'error';
            }
          })
          .then(answer => {
              if ( answer.success === true ) {
                this.dispatch({type: `${ActionTypes.EMPLOYEE_DELETE}${AsyncActionTypes.SUCCESS}`});
                this.onEmployeeGet(data.id_division);
              } else {
                throw 'error';
              }
          })
          .catch(error => {
            this.dispatch({type: `${ActionTypes.EMPLOYEE_DELETE}${AsyncActionTypes.FAILURE}`, payload: error});
          })
    }

    /**
     * @method onEmployeeAdd Запрос на добавление персонала.
     * @param {IEmployeeData} data Данные об персонале.
     */
    onEmployeeAdd = (data: IEmployeeData) => {
        this.dispatch({type: `${ActionTypes.EMPLOYEE_ADD}${AsyncActionTypes.BEGIN}`});

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data),
        }
        fetch('http://127.0.0.1:8080/createEmployee', options)
          .then(response => {
            if (response.status === 200) {
                return response.json()
            } else {
                throw 'error';
            }
          })
          .then(answer => {
              if ( answer.success === true ) {
                this.dispatch({type: `${ActionTypes.EMPLOYEE_ADD}${AsyncActionTypes.SUCCESS}`});
                this.onEmployeeGet(data.id_division);
              } else {
                throw 'error';
              }
          })
          .catch(error => {
            this.dispatch({type: `${ActionTypes.EMPLOYEE_ADD}${AsyncActionTypes.FAILURE}`, payload: error});
          })
    }

    /**
     * @method onModalOpen Открытие модального окна.
     * @param {ModalTypes} type Один из типов модального окна.
     * @param {ModalEssence} essence Один из типов сущности.
     * @param {IOrganizationData|IDivisionData|IEmployeeData|object={}} data Данные об одной из сущности, для модального окна.
     */
    onModalOpen = (type: ModalTypes, essence: ModalEssence, data: IOrganizationData|IDivisionData|IEmployeeData|object = {}) => {
        this.dispatch({type: `${ActionTypes.MODAL_OPEN}${type}${essence}`, payload: data});
    }

    /**
     * @method onModalClose Закрытие модального окна, обнуляет временные данные для окна.
     */
    onModalClose = () => {
        this.dispatch({type: `${ActionTypes.MODAL_CLOSE}`});
    }
}
